# Copyright 2016-2018 Franz Knuth, Arvid Conrad Ihrig
#                     Fritz-Haber-Institut der Max-Planck-Gesellschaft
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from __future__ import division
from builtins import map
from builtins import range
from builtins import object

import setup_paths

from nomadcore.local_meta_info import loadJsonFile, InfoKindEl
from nomadcore.parser_backend import JsonParseEventsWriterBackend
from nomadcore.parse_streamed_dicts import ParseStreamedDicts

from past.utils import old_div

# Script for simple classification of parsed Nomad structures based purely on geometric properties
# Details on the approach are described in the class ClassifyStructure

import numpy as np
import ase

import logging, os, sys

# create console handler for logging
ch = logging.StreamHandler()
logger = logging.getLogger('nomad.classify_structure')
logger.setLevel(logging.WARNING)
logger.addHandler(ch)

class ClassifyStructure(object):
    """Classifies all structures (given by section_system) in a Nomad JSON file.

    Possible classifications are: Atom, Molecule, Bulk, Surface, 2D, Polymer, 1D

    Only structures with 3 lattice vectors need an analysis. Other structures are classified immediately
    as Atom or Molecule. The classification works as follows. First, the geometric inertia tensor
    of the atoms in the unit cell and their periodic images is calculated. The three principal axes of the
    inertia tensor determine the three directions in which we look if the structure is periodic. A
    direction is considered non-periodic if the maximum distance between the spheres (given by the covalent
    radii of the individual atoms) surrounding the atoms in this direction is larger than a threshold value.
    Additionally, the thickness of the structure in the three search directions is calculated to determine
    if the structure is 1D or 2D. If there are only 2 or 1 periodic dimensions given by the metadata
    configuration_periodic_dimensions, 1 or 2 search directions are set to non-periodic, respectively. The
    methods classify, _get_pbc_search_directions, _get_pbc, _get_coverage, _get_non_periodic_direction_2D,
    and _get_non_periodic_directions_1D explain in detail how the classification is done.
    """
    UNDEFINED = 'Undefined'

    def __init__(self, section_system, periodic_repetitions = 2, threshold_vacuum = 10.0, threshold_thickness = 2.0):
        """Gets data from JSON file and sets up the ase.Atoms objects.

        Args:
            section_system: the to be analyzed section system as dictionary
            periodic_repetitions: Specifies how often the unit cell is repeated in all three directions.
                The repeated structure is then used to determine the PBC. Must be at least 2 to include
                distances between periodic images.
            vacuum_threshold: Given in Angstrom. If distance between two covalent spheres is larger
                than this value in a search direction, then this direction is considered to be non-periodic.
            threshold_thickness: A structure thicker than threshold_thickness * max(covalent_radius) is
                considered as surface instead of 2D or as polymer instead of 1D.
        """
        self.m_to_ang = 1.0e10
        self.periodic_repetitions = periodic_repetitions
        self.threshold_thickness = threshold_thickness
        self.threshold_vacuum = threshold_vacuum
        self.atoms = None
        self.atoms_repeated = None
        self.classification = None
        self.covalent_radii = ase.data.covalent_radii
        # increase covalent radius of unknown element X to 1 Angstrom
        self.covalent_radii[0] = 1.0

        # If these values cannot be read, then skip this system (by raising the Key exception)
        atom_labels = section_system["atom_labels"]
        atom_pos = section_system["atom_positions"]
        # Try to first read the cell information from the renamed metainfo
        try:
            cell = section_system["lattice_vectors"]
        except KeyError:
            cell = section_system.get("simulation_cell")
        pbc = section_system.get("configuration_periodic_dimensions")
        if pbc is None:
            pbc = np.asarray(3 * [False])
        else:
            pbc = pbc[0]
            pbc = np.array(pbc["flatData"]).reshape(pbc["shape"])
            # The pbc should be defined as a single-dimensional list
            if len(pbc.shape) == 2:
                pbc = pbc[0, :]

        # Transform data into numpy arrays, don't reshape to work with old code using flat arrays
        atom_labels = np.array(atom_labels["flatData"]).reshape(atom_labels["shape"])
        atom_pos = np.array(atom_pos["flatData"]).reshape(atom_pos["shape"])

        # No further analysis will be done if there is no cell or all periodic_dimensions are False.
        # The classification will be set to 'Atom' or 'Molecule' depending on the number of atoms.
        if cell is None or not any(pbc):
            if len(atom_pos) == 1:
                self.classification = 'Atom'
            else:
                self.classification = 'Molecule / Cluster'
        else:
            cell = np.array(cell["flatData"]).reshape(cell["shape"])
            # convert to Angstrom and numpy array
            atom_pos = self.m_to_ang * atom_pos
            cell = self.m_to_ang * cell
            # set up ase.Atoms with PBC
            if cell.shape == (3, 3):
                self.classification = None
                # check if all labels are known by ase
                # if not change unknown label to X
                for i in range(len(atom_labels)):
                    number = ase.data.atomic_numbers.get(atom_labels[i])
                    if number is None:
                        atom_labels[i] = u'X'
                # in the metadata, simulation_cell has lattice vectors as columns, ase has them as rows => transpose
                atoms = ase.Atoms(symbols=atom_labels, positions=atom_pos, cell=np.transpose(cell),
                                  pbc=pbc)
                # Shift atoms as close as possible to the centroid under periodic boundary conditions.
                # This gives the most compact structure (minimum-image convention).
                atoms.wrap(center = self._get_centroid(atoms.get_scaled_positions(wrap = False)))
                # shift centroid to origin
                atoms.set_positions(atoms.get_positions(wrap = False) - self._get_centroid(atoms.get_positions(wrap = False)))
                self.atoms = atoms
                # repeat periodic structure along the periodic dimensions
                repetitions = []
                for dimension in pbc:
                    if dimension:
                        repetitions.append(self.periodic_repetitions)
                    else:
                        repetitions.append(1)
                self.atoms_repeated = atoms.repeat(repetitions)
            else:
                logger.error("Incompatible array shape %s found for simulation cell!")
                self.classification = self.UNDEFINED

    def _get_covalent_radius(self, atomic_number):
        """Returns covalent radius in Angstrom for given atomic number.
        """
        return self.covalent_radii[atomic_number]

    def _get_centroid(self, positions):
        """Calculates centroid, i.e., center of mass with all masses set to 1.

        Args:
            positions: Position of atoms.

        Returns:
            Centroid.
        """
        return np.sum(positions, axis = 0) / len(positions)

    def _get_geometric_inertia_tensor(self, positions):
        """Calculates geometric inertia tensor, i.e., all masses are set to 1.

        I_ij = sum_k m_k (delta_ij * r_k^2 - x_ki * x_kj)
        with r_k^2 = x_k1^2 + x_k2^2 x_k3^2

        Args:
            positions: Position of atoms.

        Returns:
            Geometric inertia tensor.
        """
        # calculate centroid
        centroid = self._get_centroid(positions)
        # subtract centroid from positions
        positions = np.apply_along_axis(lambda x: x - centroid, 1, positions)
        # calculate inertia tensor
        inertia_tensor = np.zeros([3, 3], dtype = np.float64)
        for i in range(3):
            for j in range(3):
                # lower triangle
                if j < i:
                    inertia_tensor[i, j] = inertia_tensor[j, i]
                # upper triangle
                else:
                    for pos in positions:
                        if i == j:
                            # get array with elements != i from pos
                            pos_temp = pos[np.arange(len(pos)) != i]
                            inertia_tensor[i, i] += np.dot(pos_temp, pos_temp)
                        else:
                            inertia_tensor[i, j] -= pos[i] * pos[j]
        return inertia_tensor

    def _get_pbc_search_directions(self, positions):
        """Calculates the search directions for the determination of the PBC.

        The principal axes of the geometric inertia tensor are used as search directions.
        This gives an orthonormal coordinate system which allows a good scanning of the
        3-dimensional space and takes into account the geometric extent in different directions.

        Args:
            positions: Position of atoms.

        Returns:
            Search directions as matrix (row-wise) and the corresponding inverse matrix.
        """
        inertia_tensor = self._get_geometric_inertia_tensor(positions)
        logger.debug("Geometric inertia tensor in Ang^2:")
        logger.debug(inertia_tensor)
        # determine principal axes
        eigenvals, search_directions = np.linalg.eigh(inertia_tensor)
        search_directions = np.transpose(search_directions)
        logger.debug("Eigenvalues of geometric inertia tensor in Ang^2:")
        logger.debug(eigenvals)
        logger.debug("Search directions in Ang (each row is one direction):")
        logger.debug(search_directions)
        return search_directions, np.linalg.inv(search_directions)

    def _get_non_periodic_direction_2D(self, lattice_vectors, search_directions, epsilon):
        """Determines which search direction must be excluded if there are 2 periodic dimensions.

        We will exclude the search direction that is the 'most' orthogonal with respect to the plane
        given by the two periodic directions.

        Args:
            lattice_vectors: the lattice vectors of the two periodic dimensions
            search_directions: The matrix (row-wise) of the search directions.
                The vectors of the search directions must be normalized.
            epsilon: Threshold for the evaluation of the maximum of the dot products between the
                search directions and the cross product of the two lattice vectors of the periodic
                dimensions. If one or more search direction is close (<= epsilon) to the maximum,
                an error will be raised.

        Returns:
            List with the index of the excluded search direction.
        """
        # calculate the cross product of these lattice vectors and normalize
        orthogonal_direction = np.cross(lattice_vectors[0], lattice_vectors[1])
        orthogonal_direction = orthogonal_direction / np.linalg.norm(orthogonal_direction)
        # calculate dot product between search directions and orthogonal direction
        dot_product = []
        for search_direction in search_directions:
            dot_product.append(abs(np.dot(orthogonal_direction, search_direction)))
        # Get maximum dot product. This is the 'most' orthogonal direction with respect to the plane given by the two periodic directions
        max_dot_product = max(dot_product)
        max_index = [i for i, x in enumerate(dot_product) if x >= max_dot_product - epsilon]
        # check if only one maximum was found
        if len(max_index) > 1:
            logger.error("One ore more search directions are close (<= %g) to the maximum of the dot products between the search" % epsilon)
            logger.error("directions and the cross product of the two lattice vectors of the periodic dimensions given by")
            logger.error("'configuration_periodic_dimensions' in section_system with gIndex %d in section_run with")
            logger.error("gIndex %d! This message might indicate that the current method _get_non_periodic_direction_2D in the")
            logger.error("class ClassifyStructure to exclude one search direction in the case of two periodic dimensions is not")
            logger.error("applicable in general. A different method to incorporate the number of periodic dimensions with the")
            logger.error("determined search directions might be needed.")
            logger.error("Now, some debug output follows:")
            logger.error("Normalized cross product of the two lattice vectors of the periodic dimensions in Ang: %s" % orthogonal_direction)
            for i in range(len(search_directions)):
                logger.error("Search direction %d: vector in Ang:      %s" % (i, np.asarray(search_directions[i])))
                logger.error("                    abs of dot product: %g" % dot_product[i])
            sys.exit(1)
        logger.debug("Lattice vectors of the 2 periodic dimensions in Ang (each row is one vector):")
        logger.debug(lattice_vectors)
        logger.debug("Abs of dot products between the search directions and the cross product of the two lattice vectors:")
        logger.debug(np.asarray(dot_product))
        logger.debug("Excluded search direction: %d" % max_index[0])
        return max_index

    def _get_non_periodic_directions_1D(self, lattice_vector, search_directions, epsilon):
        """Determines which search directions must be excluded if there is 1 periodic dimension.

        We will include the search direction that is the 'most' parallel to the lattice vector
        given by the periodic directions. The excluded search directions are then the complementary.

        Args:
            lattice_vector: the lattice vector of the 1 periodic dimension
            search_directions: The matrix (row-wise) of the search directions.
                The vectors of the search directions must be normalized.
            epsilon: Threshold for the evaluation of the maximum of the dot products between the
                search directions and the lattice vector. If one or more search direction is
                close (<= epsilon) to the maximum, an error will be raised.

        Return :
            List with the index of the excluded search directions.
        """
        parallel_direction = lattice_vector / np.linalg.norm(lattice_vector)
        # calculate dot product between search directions and lattice vector
        dot_product = []
        for search_direction in search_directions:
            dot_product.append(abs(np.dot(parallel_direction, search_direction)))
        # Get maximum dot product. This is the 'most' parallel direction with respect to the lattice vector
        max_dot_product = max(dot_product)
        max_index = [i for i, x in enumerate(dot_product) if x >= max_dot_product - epsilon]
        # check if only one maximum was found
        if len(max_index) > 1:
            logger.error("One ore more search directions are close (<= %g) to the maximum of the dot products between the search" % epsilon)
            logger.error("directions and the lattice vector of the periodic dimension given by 'configuration_periodic_dimensions'")
            logger.error("in section_system with gIndex %d in section_run with gIndex %d! This message might indicate")
            logger.error("that the current method _get_non_periodic_direction_1D in the class ClassifyStructure to exclude one")
            logger.error("search direction in the case of one periodic dimension is not applicable in general. A different method to")
            logger.error("incorporate the number of periodic dimensions with the determined search directions might be needed.")
            logger.error("Now, some debug output follows:")
            logger.error("Parallel direction of the lattice vector of the periodic dimension in Ang: %s" % parallel_direction)
            for i in range(len(search_directions)):
                logger.error("Search direction %d: vector in Ang:      %s" % (i, np.asarray(search_directions[i])))
                logger.error("                    abs of dot product: %g" % dot_product[i])
            sys.exit(1)
        # get complementary list to max_index
        excluded_directions = list(set(range(len(search_directions))) - set(max_index))
        logger.debug("Lattice vector of the periodic dimension in Ang:")
        logger.debug(lattice_vector)
        logger.debug("Abs of dot products between the search directions and the normalized lattice vector:")
        logger.debug(np.asarray(dot_product))
        logger.debug("Excluded search directions: %s" % excluded_directions)
        return excluded_directions

    def _get_pbc(self, search_directions_inverse, positions, atomic_numbers, non_periodic_directions):
        """Determines for each search direction if the structure is periodic along this one.

        Each atom is surrounded by a sphere with its covalent radius. For each search direction,
        the atomic positions are projected on the straight line defined by the search direction.
        Thus, the spheres become intervals on this straight line. If a distance between
        these intervals is larger than threshold_vacuum, this search direction is considered
        to be non-periodic.
        In practice, the projection on the straight line defined by the search direction is done
        by transforming the positions from the Cartesian basis to the basis given by the search
        directions. Then, each of the new coordinates corresponds to the projection onto the
        individual search directions.

        Args:
            search_directions_inverse: The inverse matrix (row-wise) of the search directions.
                The vectors of the search directions must be normalized.
            positions: Position of atoms and periodic images must be included for a meaningful
                analysis and answer.
            atomic_numbers: The atomic numbers corresponding to the atoms specified in positions.
            non_periodic_directions: A list of indices which specifies search_directions that are
                set to False without analysis.

        Returns:
            List of Booleans specifying in which search directions the structure is periodic.
        """
        pbc = [None, None, None]
        # transform positions from Cartesian basis to basis given by search_directions
        scaled_positions = np.dot(positions, search_directions_inverse)
        # loop over search_directions
        for search_dir in range(len(search_directions_inverse)):
            # skip analysis for non-periodic directions and set to False
            if search_dir in non_periodic_directions:
                pbc[search_dir] = False
                continue
            # get scaled coordinates along current search direction
            scaled_positions_dir = scaled_positions[:, search_dir]
            sphere_intervals_dir = Intervals()
            # loop over each atom
            for atomic_number, atomic_position in zip(atomic_numbers, scaled_positions_dir):
                # Get covalent radius of current atom.
                # No scaling is needed because the vectors of search_directions have length 1.
                radius = self._get_covalent_radius(atomic_number)
                # Add interval that is covered by the covalent sphere surrounding
                # the current atom in the current search direction.
                sphere_intervals_dir.add_interval(atomic_position - radius, atomic_position + radius)
            logger.debug("Sphere intervals in search direction %i in Ang:" % search_dir)
            logger.debug(np.asarray(sphere_intervals_dir.get_intervals()))
            max_vacuum_distance_dir = sphere_intervals_dir.get_max_distance_between_intervals()
            logger.debug("Maximum vacuum distance in search direction %i in Ang: %f" % (search_dir, max_vacuum_distance_dir))
            # Determine if the maximum distance between the covalent spheres is less
            # than threshold_vacuum.
            if max_vacuum_distance_dir < self.threshold_vacuum:
                pbc[search_dir] = True
            else:
                pbc[search_dir] = False
        return pbc

    def _get_coverage(self, search_directions_inverse, positions, atomic_numbers):
        """Determines for each search direction how much space the covalent spheres cover.

        The projection on the straight line defined by the search direction is done as described
        in _get_pbc. Only this time, the lengths of the intervals created by the covalent spheres
        are added up and it is taken account of overlapping intervals.

        Args:
            search_directions_inverse: The inverse matrix (row-wise) of the search directions.
                The vectors of the search directions must be normalized.
            positions: Position of atoms and periodic images must not be included.
            atomic_numbers: The atomic numbers corresponding to the atoms specified in positions.

        Returns:
            List which contains for each search direction the space covered by the covalent
            spheres in Angstrom.
        """
        coverage = [None, None, None]
        scaled_positions = np.dot(positions, search_directions_inverse)
        # loop over search_directions
        for search_dir in range(len(search_directions_inverse)):
            # get scaled coordinates along current search direction
            scaled_positions_dir = scaled_positions[:, search_dir]
            sphere_intervals_dir = Intervals()
            # loop over each atom
            for atomic_number, atomic_position in zip(atomic_numbers, scaled_positions_dir):
                # Get covalent radius of current atom.
                # No scaling is needed because the vectors of search_directions have length 1.
                radius = self._get_covalent_radius(atomic_number)
                # Add interval that is covered by the covalent sphere surrounding
                # the current atom in the current search direction.
                sphere_intervals_dir.add_interval(atomic_position - radius, atomic_position + radius)
            # add up space covered by covalent spheres
            coverage[search_dir] = sphere_intervals_dir.add_up_merged_intervals()
        return coverage

    def classify(self):
        """Classifies structures.

        For structures with 3 lattice vectors, the number of periodic directions is determined
        as well as how much space the covalent spheres around the atoms cover in the non-periodic
        directions. The details how these 2 things are determined are explained in _get_pbc
        and _get_coverage. If there are only 2 or 1 periodic dimensions, 1 or 2 periodic directions
        are set to non-periodic, respectively, see _get_non_periodic_direction_2D and
        _get_non_periodic_directions_1D. According to the result of these analyses, the structure
        is classified.

        n_periodic = 0: Atom or Molecule, depending on the number of atoms
        n_periodic = 1: 1D or Polymer, depending on the thickness of the structure in the
                        non-periodic directions.
        n_periodic = 2: 2D or Surface, depending on the thickness of the structure in the
                        non-periodic direction.
        n_periodic = 3: Bulk

        Returns:
            Result as json dictionary.
            The possible classifications are: Atom, Molecule, Bulk, Surface, 2D, Polymer, 1D
        """
        # If we already classified the structure in __init__, we will do no analysis.
        if self.classification is not None:
            return
        atomic_positions = self.atoms.get_positions()
        atomic_numbers = self.atoms.get_atomic_numbers()
        # for the PBC, the atoms need to be repeated to take into account periodic images
        atomic_positions_repeated = self.atoms_repeated.get_positions()
        atomic_numbers_repeated = self.atoms_repeated.get_atomic_numbers()
        # get search directions
        search_directions, search_directions_inverse = self._get_pbc_search_directions(
            atomic_positions_repeated)
        # check how many periodic dimensions are present
        periodic_dimensions = self.atoms.get_pbc()
        n_periodic_dim = sum(1 for x in periodic_dimensions if x)
        # if there are 2 (1) periodic dimensions, exclude 1 (2) search directions
        non_periodic_directions = []
        if n_periodic_dim == 2:
            non_periodic_directions = self._get_non_periodic_direction_2D(
                self.atoms.get_cell()[periodic_dimensions], search_directions, 1.0e-1)
        elif n_periodic_dim == 1:
            non_periodic_directions = self._get_non_periodic_directions_1D(
                self.atoms.get_cell()[periodic_dimensions][0], search_directions, 1.0e-1)
        # get PBC
        pbc = self._get_pbc(search_directions_inverse, atomic_positions_repeated,
                            atomic_numbers_repeated, non_periodic_directions)
        logger.debug("PBC: %s" % pbc)
        # evaluate the PBC and classify
        n_false = sum(1 for x in pbc if not x)
        n_true = sum(1 for x in pbc if x)
        if n_true == 3 and n_false == 0:
            self.classification = 'Bulk'
        elif n_true == 0 and n_false == 3:
            if len(self.atoms) == 1:
                self.classification = 'Atom'
            else:
                self.classification = 'Molecule / Cluster'
        elif n_true == 2 and n_false == 1:
            # Determine if thickness of structure in non-periodic direction is smaller (2D) or larger (Surface)
            # than threshold_thickness * max(covalent_radius).
            index = pbc.index(False)
            coverage = self._get_coverage(search_directions_inverse, atomic_positions, atomic_numbers)
            logger.debug("Covalent spheres coverage in Ang: %s" % np.asarray(coverage))
            if coverage[index] > self.threshold_thickness * max(map(self._get_covalent_radius, atomic_numbers)):
                self.classification = 'Surface'
            else:
                self.classification = '2D'
        elif n_true == 1 and n_false == 2:
            # Determine if thickness of structure in non-periodic directions is smaller (1D) or larger (Polymer)
            # than threshold_thickness * max(covalent_radius).
            index = pbc.index(True)
            too_thick = []
            threshold = self.threshold_thickness * max(map(self._get_covalent_radius, atomic_numbers))
            coverage = self._get_coverage(search_directions_inverse, atomic_positions, atomic_numbers)
            logger.debug("Covalent spheres coverage in Ang: %s" % np.asarray(coverage))
            for i in range(len(coverage)):
                # skip periodic direction
                if i == index:
                    continue
                # evaluate threshold condition
                too_thick.append(coverage[i] > threshold)
            if any(too_thick):
                self.classification = 'Polymer'
            else:
                self.classification = '1D'
        else:
            self.classification = self.UNDEFINED

class Intervals(object):
    """Handles list of intervals.

    This class allows sorting and adding up of intervals and taking into account if they overlap.
    """
    def __init__(self, intervals = None):
        """Args:
            intervals: List of intervals that are added.
        """
        self._intervals = []
        self._merged_intervals = []
        self._merged_intervals_need_update = True
        if intervals is not None:
            self.add_intervals(intervals)

    def _add_up(self, intervals):
        """Add up the length of intervals.

        Argument:
            intervals: List of intervals that are added up.

        Returns:
            Result of addition.
        """
        if len(intervals) < 1:
            return None
        result = 0.
        for interval in intervals:
            result += abs(interval[1] - interval[0])
        return result

    def add_interval(self, a, b):
        """Add one interval.

        Args:
            a, b: Start and end of interval. The order does not matter.
        """
        self._intervals.append((min(a, b), max(a, b)))
        self._merged_intervals_need_update = True

    def add_intervals(self, intervals):
        """Add list of intervals.

        Args:
            intervals: List of intervals that are added.
        """
        for interval in intervals:
            if len(interval) == 2:
                self.add_interval(interval[0], interval[1])
            else:
                raise ValueError("Intervals must be tuples of length 2!")

    def set_intervals(self, intervals):
        """Set list of intervals.

        Args:
            intervals: List of intervals that are set.
        """
        self._intervals = []
        self.add_intervals(intervals)

    def remove_interval(self, i):
        """Remove one interval.

        Args:
            i: Index of interval that is removed.
        """
        try:
            del self._intervals[i]
            self._merged_intervals_need_update = True
        except IndexError:
            pass

    def get_intervals(self):
        """Returns the intervals.
        """
        return self._intervals

    def get_intervals_sorted_by_start(self):
        """Returns list with intervals ordered by their start.
        """
        return sorted(self._intervals, key = lambda x: x[0])

    def get_intervals_sorted_by_end(self):
        """Returns list with intervals ordered by their end.
        """
        return sorted(self._intervals, key = lambda x: x[1])

    def get_merged_intervals(self):
        """Returns list of merged intervals so that they do not overlap anymore.
        """
        if self._merged_intervals_need_update:
            if len(self._intervals) < 1:
                return self._intervals
            # sort intervals in list by their start
            sorted_by_start = self.get_intervals_sorted_by_start()
            # add first interval
            merged = [sorted_by_start[0]]
            # start from second interval
            for current in sorted_by_start[1:]:
                previous = merged[-1]
                # new interval if not current and previous are not overlapping
                if previous[1] < current[0]:
                    merged.append(current)
                # merge if current and previous are overlapping and if end of previous is expanded by end of current
                elif previous[1] < current[1]:
                     merged[-1] = (previous[0], current[1])
            self._merged_intervals = merged
            self._merged_intervals_need_update = False
        return self._merged_intervals

    def get_max_distance_between_intervals(self):
        """Returns the maximum distance between the intervals while accounting for overlap.
        """
        if len(self._intervals) < 2:
            return None
        merged_intervals = self.get_merged_intervals()
        distances = []
        if len(merged_intervals) == 1:
            return 0.0
        for i in range(len(merged_intervals) - 1):
            distances.append(abs(merged_intervals[i + 1][0] - merged_intervals[i][1]))
        return max(distances)

    def add_up_intervals(self):
        """Returns the added up lengths of intervals without accounting for overlap.
        """
        return self._add_up(self._intervals)

    def add_up_merged_intervals(self):
        """Returns the added up lengths of merged intervals in order to account for overlap.
        """
        return self._add_up(self.get_merged_intervals())


def main():
    metapath = '../../../../nomad-meta-info/meta_info/nomad_meta_info/common.nomadmetainfo.json'
    metaInfoPath = os.path.normpath(
        os.path.join(os.path.dirname(os.path.abspath(__file__)), metapath))

    metaInfoEnv, warns = loadJsonFile(filePath=metaInfoPath,
                                      dependencyLoader=None,
                                      extraArgsHandling=InfoKindEl.ADD_EXTRA_ARGS,
                                      uri=None)
    backend = JsonParseEventsWriterBackend(metaInfoEnv)
    calcContext = sys.argv[1]
    backend.startedParsingSession(
        calcContext,
        parserInfo={'name': 'SystemTypeNormalizer', 'version': '1.0'}
    )

    dict_stream = ParseStreamedDicts(sys.stdin)

    while True:

        section_system = dict_stream.readNextDict()
        if section_system is None:
            break

        try:
            structure = ClassifyStructure(section_system)
            structure.classify()
            structure_type = structure.classification

        # If these values could not be read, then skip this system
        except KeyError:
            logger.error("section %s didn't have all required meta data" % section_system["uri"])
            continue

        context = section_system["uri"]
        backend.openContext(context)
        backend.addValue("system_type", structure_type)
        backend.closeContext(context)
        sys.stdout.flush()

    backend.finishedParsingSession("ParseSuccess", None)
    sys.stdout.flush()


if __name__ == '__main__':
    main()
